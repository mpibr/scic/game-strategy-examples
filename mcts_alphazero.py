# main.py
import tictactoe as ttt
import random
import math
import logging
import os
import numpy as np
import torch
from cnn_model_pytorch import TicTacToeCNN

# Initialize the CNN model
model = TicTacToeCNN()
model.eval()  # Set the model to evaluation mode

def check_for_immediate_win(board, player):
    for (i, j) in ttt.get_possible_moves(board):
        temp_board = [row[:] for row in board]
        temp_board[i][j] = player
        if ttt.is_winner(temp_board, player):
            return (i, j)
    return None

# def game_result(board):
#     if ttt.is_winner(board, player_one):
#         logging.info("Player " + player_one + " won")
#         return 1
#     elif ttt.is_winner(board, player_two):
#         logging.info("Player " + player_two + " won")
#         return -1
#     else:
#         return 0

def board_to_input(board, player):
    input_board = np.zeros((1, 3, 3))
    for i in range(3):
        for j in range(3):
            if board[i][j] == player:
                input_board[0, i, j] = 1
            elif board[i][j] != " ":
                input_board[0, i, j] = -1
    return torch.tensor(input_board, dtype=torch.float).unsqueeze(0)

def simulate(board, player_one, player_two):
    board_input = board_to_input(board, player_one)
    _, value = model(board_input)
    return value.item()

C_PUCT = 1.0

def select_child(node):
    return max(node['children'], key=lambda x: x['wins'] / x['visits'] + C_PUCT * x['policy'] * math.sqrt(node['visits']) / (1 + x['visits']))

def mcts(board, player_one, player_two, iterations):
    board_input = board_to_input(board, player_one)
    root_policy, _ = model(board_input)
    root_policy = root_policy.detach().numpy().flatten()  # Convert to numpy array and flatten
    
    root = {'board': [row[:] for row in board], 'player': player_one, 'wins': 0, 'visits': 0, 'children': [], 'untried_actions': ttt.get_possible_moves(board), 'policy': root_policy}
    
    for _ in range(iterations):
        node = root
        path = [node]
        
        while not node['untried_actions'] and node['children']:
            node = select_child(node)
            path.append(node)
        
        if node['untried_actions']:
            move = node['untried_actions'].pop()
            new_board = [row[:] for row in node['board']]
            new_board[move[0]][move[1]] = node['player']
            next_player = player_two if node['player'] == player_one else player_one
            child_board_input = board_to_input(new_board, next_player)
            child_policy, _ = model(child_board_input)
            child_policy = child_policy.detach().numpy().flatten()
            
            child = {'board': new_board, 'player': next_player, 'wins': 0, 'visits': 0, 'children': [], 'untried_actions': ttt.get_possible_moves(new_board), 'policy': child_policy}
            node['children'].append(child)
            node = child
            path.append(node)
        
        result = simulate([row[:] for row in node['board']], player_one, player_two)
        
        for node in path:
            node['visits'] += 1
            if node['player'] == player_one:
                node['wins'] += result
            else:
                node['wins'] -= result
    return root

def find_best_move(board, player_one, player_two, iterations=10):
    #MCTS misses initial wins in its basic form
    immediate_win = check_for_immediate_win(board, player_one)
    if immediate_win:
        logging.info("Immediate win for player " + str(player_one) + ", skipping MCTS")
        return immediate_win

    # Check if the opponent has an immediate winning move and block it.
    opponent_win = check_for_immediate_win(board, player_two)
    if opponent_win:
        logging.info("Blocking immediate win for player " + str(player_two) + ", skipping MCTS")
        return opponent_win

    root = mcts(board, player_one, player_two, iterations)
    best_move = max(root['children'], key=lambda x: x['visits'])['board']
    
    # Retrieve coordinates of changes of board
    num_rows = len(board)
    num_cols = len(board[0])
    for i in range(num_rows):
        for j in range(num_cols):
            if board[i][j] != best_move[i][j]:
                return i, j

def main():
    os.system('clear')
    
    #logging.basicConfig(level=logging.INFO) #activate logging to see all intermediate steps

    player_one = "O"
    player_two = "X"

    board = [["O", " ", " ", " "],
            [" ", "O", " ", " "],
            [" ", "O", " ", " "],
            ["X", "X", " ", "O"]]

    print("Initial board, next player: " + str(player_one))
    print(ttt.get_ascii_board(board))

    i, j = find_best_move(board, player_one, player_two)
    board[i][j] = player_one + player_one #double symbol changes color
    
    print("Players " + str(player_one) + " best move:")
    print(ttt.get_ascii_board(board))

if __name__ == "__main__":
    main()