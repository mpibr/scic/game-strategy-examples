import os
import tictactoe as ttt
import logging

def minimax(board, depth, is_maximizing, maximizing_player, minimizing_player):
    #Print out the PREVIOUS board state
    logging.info((("Player " + str(maximizing_player) + " maximized, ") if not is_maximizing else ("Player " + str(minimizing_player) + " minimized, ")) + "depth: " + str(depth))
    logging.info(ttt.get_ascii_board(board))

    if ttt.is_winner(board, maximizing_player):
        score = 10 - depth
        logging.info(str(maximizing_player) + " Won! Score: " + str(score) + "\n")
        return score
    elif ttt.is_winner(board, minimizing_player):
        score = -10 + depth
        logging.info(str(minimizing_player) + " Won! Score: " + str(score) + "\n")
        return score
    elif ttt.is_draw(board):
        logging.info("Draw! Score: 0")
        return 0

    if is_maximizing:
        best_score = -float("inf")
        for (i, j) in ttt.get_possible_moves(board):
            board[i][j] = maximizing_player.lower()
            score = minimax(board, depth + 1, False, maximizing_player, minimizing_player) # Maximize next
            board[i][j] = " " # Revert last move
            best_score = max(score, best_score)
        return best_score
    else: # is_minimizing
        best_score = float("inf")
        for (i, j) in ttt.get_possible_moves(board):
            board[i][j] = minimizing_player.lower()
            score = minimax(board, depth + 1, True, maximizing_player, minimizing_player) # Minimize next
            board[i][j] = " " # Revert last move
            best_score = min(score, best_score)
        return best_score

def find_best_move(board, maximizing_player, minimizing_player):
    best_score = -float("inf")
    best_move = None
    
    for (i, j) in ttt.get_possible_moves(board):
        board[i][j] = maximizing_player.lower()
        score = minimax(board, 0, False, maximizing_player, minimizing_player)

        board[i][j] = " "
        if score > best_score:
            best_score = score
            best_move = (i, j)
    return best_move


def main():
    os.system('clear')
    
    #logging.basicConfig(level=logging.INFO) #activate logging to see all intermediate steps

    player_one = "O"
    player_two = "X"

    board = [["O", " ", " ", " "],
            [" ", "O", " ", " "],
            [" ", "O", "O", " "],
            ["X", "X", " ", "O"]]

    print("Initial board, next player: " + str(player_one))
    print(ttt.get_ascii_board(board))

    i, j = find_best_move(board, player_one, player_two)
    board[i][j] = player_one + player_one #double symbol changes color
    
    print("Players " + str(player_one) + " best move:")
    print(ttt.get_ascii_board(board))


if __name__ == "__main__":
    main()