import tictactoe as ttt
import minimax
import minimax_alpha_beta
import mcts
import os, sys
import logging


# Configure logging to print immediately. Desireable for recursive printing
logging.basicConfig(level=logging.INFO)

os.system('clear')
player_one = "O"
player_two = "X"

board = [["O", " ", " ", " "],
         ["X", "O", "X", "O"],
         [" ", "O", "O", " "],
         ["X", "X", " ", "O"]]

print("Initial board, next player: " + str(player_one))
print(ttt.get_ascii_board(board))


i, j = minimax.find_best_move(board, player_one, player_two)
#i, j = minimax_alpha_beta.find_best_move(board, player_one, player_two)
#i, j = mcts.find_best_move(board, player_one, player_two)
board[i][j] = player_one + player_one
logging.info("\nPlayers " + str(player_one) + " best move:")
print(ttt.get_ascii_board(board))



# os.system('clear')
# board = [["X", " ", "X"],
#          ["O", " ", " "],
#          ["O", "X", " "]]

# minimizing_player = "X"
# maximizing_player = "O"

# print("Initial board, next player: " + str(maximizing_player), flush=True)
# ttt.print_board(board)

# i, j = find_best_move(board, maximizing_player, minimizing_player)
# board[i][j] = maximizing_player + maximizing_player
# print("\nPlayer " + str(maximizing_player) + "'s best move:", flush=True)
# ttt.print_board(board)
# player = "X"
# round = 1
# while not (somebody_won or is_board_full(board)):
#     # AI makes its move
#     best_move = find_best_move(board, player)
#     if best_move is not None:
#         board[best_move[0]][best_move[1]] = player
#     if is_winner(board, player):
#         somebody_won = True
 
#     print("\nROUND: " + str(round) + "\n")
#     print_board(board)
    
#     round = round + 1
#     player = "O" if player == "X" else "X"
    