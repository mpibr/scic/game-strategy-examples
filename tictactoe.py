def get_ascii_board(board):
    green = '\033[92m'  # ANSI escape code for green
    magenta = '\033[95m'  # ANSI escape code for magenta
    red = '\033[91m'  # ANSI escape code for red
    reset = '\033[0m'  # ANSI escape code to reset color

    num_rows = len(board)
    num_cols = len(board[0])

    # Use Unicode box drawing characters for vertical lines
    vertical_line = "│"

    # Colorization  X: default, x: magenta, XX: green, xx: red
    ascii_board = "\n"
    for i, row in enumerate(board):
        row_str = ""
        for cell in row:
            if len(cell) == 1:
                if cell.islower():
                    colorized_cell = f"{magenta}{cell.upper()}{reset}"
                else:  # Uppercase single character
                    colorized_cell = cell.upper()  # No color change for uppercase single character
            elif len(cell) == 2:
                if cell.isupper():
                    colorized_cell = f"{green}{cell[0]}{reset}"
                elif cell.islower():
                    colorized_cell = f"{red}{cell[0].upper()}{reset}"
                else:
                    colorized_cell = cell  # Default case if not all upper or lower
            else:
                colorized_cell = cell  # Default case for cell length not 1 or 2

            if row_str:  # Add vertical line separator between cells
                row_str += vertical_line
            row_str += f" {colorized_cell} "

        ascii_board += row_str

        if i < num_rows-1:  # Only print the row separator if it's not the last row
            ascii_board += "\n" + "───" + ("┼───" * (num_cols-1))
        
        ascii_board += "\n"

    return ascii_board

def is_winner(board, player):
    num_rows = len(board)
    num_cols = len(board[0])

    # Check rows, columns for a win
    for i in range(num_rows):
        # Check if all elements in row i are the same as player
        if all(board[i][j].upper() == player.upper() for j in range(num_cols)):
            return True
        # Check if all elements in column i are the same as player
        if all(board[j][i].upper() == player.upper() for j in range(num_rows)):
            return True

    # Check diagonal for a win
    diagonal_length = min(num_rows, num_cols)
    primary_diagonal_win = all(board[i][i] == player for i in range(diagonal_length))
    secondary_diagonal_win = all(board[i][num_cols-i-1] == player for i in range(diagonal_length))
    if primary_diagonal_win or secondary_diagonal_win: 
        return True

    return False

def is_draw(board):
    num_rows = len(board)
    num_cols = len(board[0])
    if all(board[i][j] != " " for i in range(num_rows) for j in range(num_cols)):
        return True
    else:
        return False

def is_board_full(board):
    num_rows = len(board)
    num_cols = len(board[0])
    if all(board[i][j] != " " for i in range(num_rows) for j in range(num_cols)):
        return True
    else:
        return False

def get_possible_moves(board):
    num_rows = len(board)
    num_cols = len(board[0])
    return [(i, j) for i in range(num_rows) for j in range(num_cols) if board[i][j] == " "]
