import tictactoe as ttt
import random
import math
import logging
import os

def check_for_immediate_win(board, player):
    for (i, j) in ttt.get_possible_moves(board):
        temp_board = [row[:] for row in board]
        temp_board[i][j] = player
        if ttt.is_winner(temp_board, player):
            return (i, j)
    return None

def game_result(board):
    if ttt.is_winner(board, player_one):
        logging.info("Player " + player_one + " won")
        return 1
    elif ttt.is_winner(board, player_two):
        logging.info("Player " + player_two + " won")
        return -1
    else:
        return 0

def simulate(board, player_one, player_two):
    current_player = player_one
    depth = 0
    while True:
        if ttt.is_winner(board, current_player):
            logging.info("Player " + str(current_player) + " won!")
            return 1 if current_player == player_one else -1
        if ttt.is_draw(board):
            logging.info("Draw!")
            return 0
        possible_moves = ttt.get_possible_moves(board)
        move = random.choice(possible_moves)
        board[move[0]][move[1]] = current_player.lower()
        logging.info("Simulating player " + str(current_player))
        ttt.print_board(board)
        current_player = player_two if current_player == player_one else player_one

def mcts(board, player_one, player_two, iterations):
    root = {'board': [row[:] for row in board], 'player': player_one, 'wins': 0, 'visits': 0, 'children': [], 'untried_actions': ttt.get_possible_moves(board)}
    #logging.info(root)
    for _ in range(iterations):
        node = root
        path = [node]
        #logging.info("Selection: root node")
        #logging.info(node)
        # Selection
        while not node['untried_actions'] and node['children']:
            node = max(node['children'], key=lambda x: x['wins'] / x['visits'] + math.sqrt(2 * math.log(node['visits']) / x['visits']))
            path.append(node)
            #logging.info("Selection:")
            #logging.info(node)
        # Expansion
        if node['untried_actions']:
            move = node['untried_actions'].pop()
            new_board = [row[:] for row in node['board']]
            new_board[move[0]][move[1]] = node['player']
            next_player = player_two if node['player'] == player_one else player_one
            child = {'board': new_board, 'player': next_player, 'wins': 0, 'visits': 0, 'children': [], 'untried_actions': ttt.get_possible_moves(new_board)}
            node['children'].append(child)
            node = child
            path.append(node)
            #logging.info("Expansion:")
            #logging.info(node)
        # Simulation
        result = simulate([row[:] for row in node['board']], player_one, player_two)
        #logging.info("Simulation result: " + str(result))
        # Backpropagation
        for node in path:
            node['visits'] += 1
            if node['player'] == player_one:
                node['wins'] += result
            else:
                node['wins'] -= result
    return root

def find_best_move(board, player_one, player_two, iterations=10):
    #MCTS misses initial wins in its basic form
    immediate_win = check_for_immediate_win(board, player_one)
    if immediate_win:
        logging.info("Immediate win for player " + str(player_one) + ", skipping MCTS")
        return immediate_win

    # Check if the opponent has an immediate winning move and block it.
    opponent_win = check_for_immediate_win(board, player_two)
    if opponent_win:
        logging.info("Blocking immediate win for player " + str(player_two) + ", skipping MCTS")
        return opponent_win

    root = mcts(board, player_one, player_two, iterations)
    best_move = max(root['children'], key=lambda x: x['visits'])['board']
    
    # Retrieve coordinates of changes of board
    num_rows = len(board)
    num_cols = len(board[0])
    for i in range(num_rows):
        for j in range(num_cols):
            if board[i][j] != best_move[i][j]:
                return i, j

def main():
    os.system('clear')
    
    #logging.basicConfig(level=logging.INFO) #activate logging to see all intermediate steps

    player_one = "O"
    player_two = "X"

    board = [["O", " ", " ", " "],
            [" ", "O", " ", " "],
            [" ", "O", " ", " "],
            ["X", "X", " ", "O"]]

    print("Initial board, next player: " + str(player_one))
    print(ttt.get_ascii_board(board))

    i, j = find_best_move(board, player_one, player_two)
    board[i][j] = player_one + player_one #double symbol changes color
    
    print("Players " + str(player_one) + " best move:")
    print(ttt.get_ascii_board(board))

if __name__ == "__main__":
    main()